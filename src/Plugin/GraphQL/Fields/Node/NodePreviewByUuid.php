<?php

namespace Drupal\graphql_node_preview\Plugin\GraphQL\Fields\Node;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\TypedData\TranslatableInterface;
use Drupal\graphql\GraphQL\Cache\CacheableValue;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\graphql_node_preview\GraphQL\Buffers\NodePreviewBuffer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * NodePreviewByUuid field.
 *
 * @GraphQLField(
 *   id = "node_preview_by_uuid",
 *   secure = true,
 *   arguments = {
 *     "uuid" = "String!"
 *   },
 *   contextual_arguments = {"language"},
 *   deriver = "Drupal\graphql_node_preview\Plugin\Deriver\Fields\NodePreviewByUuidDeriver"
 * )
 */
class NodePreviewByUuid extends FieldPluginBase implements ContainerFactoryPluginInterface {
  use DependencySerializationTrait;

  /**
   * The node preview buffer service.
   *
   * @var \Drupal\graphql_node_preview\GraphQL\Buffers\NodePreviewBuffer
   */
  protected $nodePreviewBuffer;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager'),
      $container->get('entity.repository'),
      $container->get('graphql_node_preview.buffer.node_preview')
    );
  }

  /**
   * EntityById constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   The entity repository service.
   * @param \Drupal\graphql_node_preview\GraphQL\Buffers\NodePreviewBuffer $nodePreviewBuffer
   *   The entity buffer service.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    EntityRepositoryInterface $entityRepository,
    NodePreviewBuffer $nodePreviewBuffer
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->nodePreviewBuffer = $nodePreviewBuffer;
    $this->entityTypeManager = $entityTypeManager;
    $this->entityRepository = $entityRepository;
  }

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    $resolver = $this->nodePreviewBuffer->add($this->getPluginDefinition()['entity_type'], $args['uuid']);
    return function ($value, array $args, ResolveContext $context, ResolveInfo $info) use ($resolver) {
      if (!$entity = $resolver()) {
        // If there is no entity with this id, add the list cache tags so that
        // the cache entry is purged whenever a new entity of this type is
        // saved.
        $pluginDefinition = $this->getPluginDefinition();
        $entityType = $this->entityTypeManager->getDefinition($pluginDefinition['entity_type']);
        yield (new CacheableValue(NULL))->addCacheTags($entityType->getListCacheTags());
      }
      else {
        /** @var \Drupal\Core\Entity\EntityInterface $entity */
        $access = $entity->access('view', NULL, TRUE);

        if ($access->isAllowed()) {
          if (isset($args['language']) && $args['language'] != $entity->language()->getId() && $entity instanceof TranslatableInterface && $entity->isTranslatable()) {
            if ($entity->hasTranslation($args['language'])) {
              $entity = $entity->getTranslation($args['language']);
            }
          }

          // Since this is a preview operation, it shouldn't be cached.
          yield $entity->mergeCacheMaxAge(0);
        }
        else {
          yield new CacheableValue(NULL, [$access]);
        }
      }
    };
  }

}
