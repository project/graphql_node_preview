<?php

namespace Drupal\graphql_node_preview\Plugin\GraphQL\Fields\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * @GraphQLField(
 *   id = "entity_url",
 *   secure = true,
 *   name = "entityUrl",
 *   type = "Url",
 *   parents = {"Entity"}
 * )
 */
class EntityUrl extends FieldPluginBase {

  /**
   * {@inheritdoc}
   *
   * Override Drupal\graphql_core\Plugin\GraphQL\Fields\Entity\EntityUrl to
   * check if entity has an ID before trying to get its URL. Not doing so leads
   * to an Exception when previewing a node that hasn't already be saved for the
   * first time.
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof EntityInterface && $value->id()) {
      yield $value->toUrl();
    }
  }

}
