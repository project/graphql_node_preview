<?php

namespace Drupal\graphql_node_preview\GraphQL\Buffers;

use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\graphql\GraphQL\Buffers\BufferBase;
use Drupal\node\ProxyClass\ParamConverter\NodePreviewConverter;

/**
 * NodePreviewBuffer class.
 */
class NodePreviewBuffer extends BufferBase {

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The node preview converter.
   *
   * @var \Drupal\node\ProxyClass\ParamConverter\NodePreviewConverter
   */
  protected $nodePreviewConverter;

  /**
   * NodePreviewBuffer constructor.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entityRepository
   *   The entity repository service.
   * @param \Drupal\node\ProxyClass\ParamConverter\NodePreviewConverter $nodePreviewConverter
   *   The node preview converter.
   */
  public function __construct(EntityRepositoryInterface $entityRepository, NodePreviewConverter $nodePreviewConverter) {
    $this->entityRepository = $entityRepository;
    $this->nodePreviewConverter = $nodePreviewConverter;
  }

  /**
   * Add an item to the buffer.
   *
   * @param string $type
   *   The entity type of the given entity ids.
   * @param array|int $id
   *   The entity id(s) to load.
   *
   * @return \Closure
   *   The callback to invoke to load the result for this buffer item.
   */
  public function add($type, $id) {
    $item = new \ArrayObject([
      'type' => $type,
      'id' => $id,
    ]);

    return $this->createBufferResolver($item);
  }

  /**
   * {@inheritdoc}
   */
  protected function getBufferId($item) {
    return $item['type'];
  }

  /**
   * {@inheritdoc}
   */
  public function resolveBufferArray(array $buffer) {
    $type = reset($buffer)['type'];
    $ids = array_map(function (\ArrayObject $item) {
      return (array) $item['id'];
    }, $buffer);

    $ids = call_user_func_array('array_merge', $ids);
    $ids = array_values(array_unique($ids));

    $entity = $this->nodePreviewConverter->convert($ids[0], NULL, 'node_preview', []);
    // If no preview data is found, load its entity.
    if (!$entity) {
      try {
        $entity = $this->entityRepository
          ->loadEntityByUuid($type, $ids[0]);
      }
      catch (EntityStorageException $e) {
        $entity = NULL;
      }
    }
    $entities = [$ids[0] => $entity];

    return array_map(function ($item) use ($entities) {
      return isset($entities[$item['id']]) ? $entities[$item['id']] : NULL;
    }, $buffer);
  }

}
