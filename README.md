# GraphQL Node Preview

A module to get node previews using GraphQL.

## INTRODUCTION ##
Install it and it works. There's no configuration, no UI.

It adds a `nodePreviewByUuid` query to GraphQL, to be able to obtain a node
preview using entity's UUID. The expected workflow to make this module work is
as following:

* Edit any node and click on "Preview" button
* A new window opens, with a URL like this:
  http://example.com/node/preview/1f8b435a-0026-4628-93f3-38ae7353ffbb/full
* Grab the UUID (the "8-4-4-4-12" chars, "1f8b435a-0026-4628-93f3-38ae7353ffbb"
  in this example)
* Execute a GraphQL query like this one:

```graphql
{
  preview:nodePreviewByUuid(uuid: "1f8b435a-0026-4628-93f3-38ae7353ffbb") {
    entityId
    entityType
    ...
  }
}
```

Being a preview operation, `nodePreviewByUuid()` is not cached.

You can use any UUID to query `nodePreviewByUuid()`. If that UUID has not been
previewed,  `nodePreviewByUuid()` works just the same as `nodeById()` query,
retrieving the entity associated with that UUID. Please note that
`nodePreviewByUuid()` is not cached, so do not use it as a substitute of
`nodeById()`.

## REQUIREMENTS ##
GraphQL module

## INSTALLATION ##
Simply run `composer require drupal/graphql_node_preview`.

## CONFIGURATION ##
Install it and it works. There's no configuration, no UI.
